import Vue from 'vue';
import Router from 'vue-router';
import * as fromViews from './views';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/my-cards',
      // name: 'my-cards',
      // component: () => import(/* webpackChunkName: "my-cards" */ './views/my-cards.vue'),
    },
    {
      path: '/my-cards',
      name: 'my-cards',
      component: () => import(/* webpackChunkName: "my-cards" */ './views/my-cards.vue'),
    },
    
    
    // {
    //   path: '/selectService',
    //   name: 'selectService',
    //   component: fromViews.SelectServiceComponent,
    // }
  ],
});
